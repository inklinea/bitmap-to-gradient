#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Bitmap to Gradient - Create svg gradients from bitmap images
# An Inkscape 1.1+ extension
##############################################################################

import inkex
from inkex import Group

import csv

import random
import math

import base64
import time
import os

from io import BytesIO
from lxml import etree

from PIL import Image

conversions = {
    'in': 96.0,
    'pt': 1.3333333333333333,
    'px': 1.0,
    'mm': 3.779527559055118,
    'cm': 37.79527559055118,
    'm': 3779.527559055118,
    'km': 3779527.559055118,
    'Q': 0.94488188976378,
    'pc': 16.0,
    'yd': 3456.0,
    'ft': 1152.0,
    '': 1.0,  # Default px
}


# Could not find simplestyle, found this instead in extensions repo
def formatStyle(a):
    """Format an inline style attribute from a dictionary"""
    return ";".join([att + ":" + str(val) for att, val in a.items()])


def get_attributes(self):
    for att in dir(self):
        try:
            inkex.errormsg((att, getattr(self, att)))
        except:
            None


def create_new_group(self, prefix, mode, suffix_type='epoch'):
    if suffix_type == 'random':
        id_suffix = str(random.randrange(1000000, 9999999))
    if suffix_type == 'epoch':
        id_suffix = str(time.time())

    group_id = str(prefix) + '_' + id_suffix
    new_group = self.svg.add(Group.new(group_id))
    new_group.set('inkscape:groupmode', str(mode))
    new_group.attrib['id'] = group_id

    return new_group


def open_bitmap(self, import_bitmap_filepath):
    try:
        im = Image.open(import_bitmap_filepath)
    except:
        inkex.errormsg('Please Select a Valid Bitmap File')
        return

    process_image(self, im)

    im.close()


def embedded_image_to_PIL(self, embedded_image):
    my_href = embedded_image.get('xlink:href')
    base64_string = my_href.split('base64,')[1]
    img_stream = BytesIO()
    img_stream.write(base64.b64decode(base64_string))
    im = Image.open(img_stream)
    im.save(img_stream, format='PNG')
    # inkex.errormsg((im.format, im.size, im.mode))

    process_image(self, im)


def process_image(self, im):
    # Convert GIF to rgba if has transparency
    # convert rgba images to rgb if transparency not wanted
    # colour to replace transparency can be chosen by user

    transparency_bool = self.options.alpha_cb

    if im.format == 'GIF':
        if 'transparency' in im.info:
            # inkex.errormsg('Transparent GIF')
            im = im.convert(mode='RGBA')
            img_stream = BytesIO()
            im.save(img_stream, format='PNG')

        else:

            im = im.convert(mode='RGB')

    if im.format == 'TIFF':
        im = im.convert(mode='RGB')

    # for any other non rgb or non rgba image
    # covert to rgb
    if im.mode != 'RGB' and im.mode != 'RGBA':
        im = im.convert(mode='RGB')

    if self.options.alpha_cb != 'true' and im.mode == 'RGBA':
        im = rgba_to_rgb(self, im)

    if self.options.greyscale_cb == 'true':
        im = im.convert('LA')
        im = im.convert(mode='RGBA')

    my_image_tag = base64_image_to_canvas(self, self.svg, 'my_image', im)

    gradient_preview_group = make_gradient_preview_group(self, my_image_tag, im)

    make_color_list(self, im)

    # inkex.errormsg((im.format, im.size, im.mode))


def make_gradient_preview_group(self, my_image_tag, im):
    preview_parent_group = create_new_group(self, 'image_preview_layer', 'layer')
    # Add attribute to allow deletion later
    preview_parent_group.attrib['bitmap_to_gradient_preview_layer'] = 'true'
    preview_image_group = create_new_group(self, 'image_preview', 'layer')
    preview_scanline_group = create_new_group(self, 'scanline_preview', 'layer')
    preview_gradient_rectangle_group = create_new_group(self, 'gradient_rectangle_preview', 'layer')
    preview_gradient_circle_group = create_new_group(self, 'gradient_circle_preview', 'layer')
    preview_stop_count_group = create_new_group(self, 'stop_count_preview', 'layer')

    preview_image_group.append(my_image_tag)
    preview_parent_group.append(preview_image_group)
    preview_parent_group.append(preview_scanline_group)
    preview_parent_group.append(preview_gradient_rectangle_group)
    preview_parent_group.append(preview_gradient_circle_group)
    preview_parent_group.append(preview_stop_count_group)

    return gradient_preview_group.extend((preview_parent_group, preview_image_group, preview_scanline_group,
                                          preview_gradient_rectangle_group, preview_stop_count_group))


def make_color_list(self, im):
    color_list = []

    px = im.load()

    gradient_lower_threshold_percentage = self.options.gradient_lower_threshold
    gradient_upper_threshold_percentage = self.options.gradient_upper_threshold

    gradient_lower_threshold = int((im.size[0] / 100) * gradient_lower_threshold_percentage)
    gradient_upper_threshold = int((im.size[0] / 100) * gradient_upper_threshold_percentage)

    if gradient_lower_threshold > gradient_upper_threshold:
        inkex.errormsg(f'Right side of scanline must be larger than left side')
        return

    image_y_offset_percentage = self.options.image_y_offset

    image_y_offset_value = int((im.size[1] / 100) * image_y_offset_percentage)

    for p_x in range(gradient_lower_threshold, gradient_upper_threshold):
        color_list.append(list(px[p_x, image_y_offset_value]))

    # Process color list

    processed_color_list = process_color_list(self, color_list)

    linear_gradient_object = make_linear_gradient(self, processed_color_list)

    make_scanline(self, im, gradient_lower_threshold, gradient_upper_threshold, image_y_offset_value)

    make_preview_gradient_rectangle(self, linear_gradient_object)
    make_preview_gradient_circle(self, linear_gradient_object)

    ### Check if we need to save the gradient to .csv

    if self.options.export_csv_cb == 'true':
        export_csv(self, color_list)


def export_csv(self, color_list):
    csv_name = self.options.export_csv_filepath.split('.')[0]

    try:
        try_file = open(csv_name, 'w')
        try_file.close()
        os.remove(csv_name)
    except:
        inkex.errormsg('filepath is not valid ( or non writeable ) ')
        return

    filename_suffix = (str(time.time())).replace('.', '')

    with open(f'{csv_name}_{filename_suffix}.csv', 'w', newline='') as csvfile:
        color_writer = csv.writer(csvfile, delimiter='|',
                                  quotechar='"', quoting=csv.QUOTE_ALL)

        for color in color_list:
            single_row_list = []
            rgb_color = f'rgb{color[0], color[1], color[2]}'
            single_row_list.append(rgb_color)
            color_writer.writerow(single_row_list)


def process_color_list(self, color_list):
    stop_density_percentage = self.options.stop_density

    # sample the list according to stop density

    if stop_density_percentage < 100 and len(color_list) > 1:

        nth_item_removal = math.ceil(100 / stop_density_percentage)

        reduced_color_list = []

        for nth_item in range(0, len(color_list) - 1, nth_item_removal):
            reduced_color_list.append(color_list[nth_item])

        color_list = reduced_color_list

    number_of_stops = len(color_list)

    percentage_increment = 100 / number_of_stops
    stop_offset = 0

    color_list_index = 1

    if self.options.reverse_cb == 'true':
        color_list.reverse()

    # Establish 1st stop
    processed_color_list = []
    processed_color_entry = color_list[0]
    processed_color_entry.append(f'{0}%')
    processed_color_list.append(processed_color_entry)

    stop_offset += percentage_increment

    # inkex.errormsg(color_list)

    if self.options.optimise_cb == 'true':
        for entry in color_list[1:-1]:
            # inkex.errormsg(f'\n Entry {entry} \n +1 {color_list[color_list_index + 1]} \n -1 {color_list[color_list_index - 1]}')
            if entry == color_list[color_list_index + 1] and entry == color_list[color_list_index - 1]:
                None
            else:
                processed_color_entry = entry.copy()
                processed_color_entry.append(f'{stop_offset}%')

                processed_color_list.append(processed_color_entry)

            color_list_index += 1
            stop_offset += percentage_increment
        # Add last item to avoid color banding
        processed_color_entry = color_list[-1]
        processed_color_entry.append(f'{100}%')
        processed_color_list.append(processed_color_entry)

    else:
        for entry in color_list[1:-1]:
            processed_color_entry = entry
            processed_color_entry.append(f'{stop_offset}%')
            processed_color_list.append(processed_color_entry)

            color_list_index += 1
            stop_offset += percentage_increment

    return processed_color_list


def base64_image_to_canvas(self, parent, base_id, im):
    img_stream = BytesIO()
    im.save(img_stream, format='PNG')
    byte_img = img_stream.getvalue()
    base64_img_str = base64.b64encode(byte_img)

    my_image = etree.SubElement(parent, inkex.addNS('image', 'svg'))
    # Lets place the preview image off the canvas
    my_image.attrib['x'] = str(0)
    my_image.attrib['y'] = str((-(im.size[1]) / conversion_factor))

    my_image.attrib['width'] = str(im.size[0] / conversion_factor)
    my_image.attrib['height'] = str(im.size[1] / conversion_factor)

    # my_image.attrib['width'] = str('500px')
    # my_image.attrib['width'] = str('200px')

    my_image.attrib['id'] = f'{base_id}'

    my_image.set('xlink:href', str(f'data:image/png;base64,{base64_img_str.decode()}'))

    return my_image


def make_scanline(self, im, gradient_lower_threshold, gradient_upper_threshold, image_y_offset_value):
    parent = gradient_preview_group[2]

    my_scanline_image_y_offset = (((-(im.size[1]) + (image_y_offset_value))))

    my_scanline_points = [gradient_lower_threshold, my_scanline_image_y_offset, gradient_upper_threshold,
                          my_scanline_image_y_offset]

    my_scanline_path = etree.SubElement(parent, inkex.addNS('path', 'svg'))

    my_scanline_path.attrib[
        'd'] = f'M {gradient_lower_threshold / conversion_factor} {my_scanline_image_y_offset / conversion_factor} {gradient_upper_threshold / conversion_factor} {(my_scanline_image_y_offset / conversion_factor)}'

    if im.size[1] > 1:
        my_scanline_path.style['stroke-width'] = '1px'
    else:
        my_scanline_path.style['stroke-width'] = '0.25px'

    my_scanline_path.style['stroke'] = self.options.color_picker_scanline


def make_preview_gradient_rectangle(self, linear_gradient_object):
    parent = gradient_preview_group[3]

    height = 100 / conversion_factor
    width = 400 / conversion_factor
    x = 10 / conversion_factor
    y = 10 / conversion_factor

    style = {'stroke': 'black',
             'stroke-width': '0.5',
             'fill': 'none',
             'stroke-dasharray': '1,1'
             }

    attribs = {
        'style': formatStyle(style),
        'height': str(height),
        'width': str(width),
        'd': f'M 5 5 100 5 100 45 5 45 Z'

    }

    my_rect_path = etree.SubElement(parent, inkex.addNS('path', 'svg'), attribs)
    my_rect_path.style['fill'] = f'url(#{linear_gradient_object.get_id()})'


def make_preview_gradient_circle(self, linear_gradient_object):
    parent = gradient_preview_group[3]

    id_suffix = str(random.randrange(1000000, 9999999))

    height = 100 / conversion_factor
    width = 400 / conversion_factor
    x = 10 / conversion_factor
    y = 10 / conversion_factor

    style = {'stroke': 'black',
             'stroke-width': '0.5',
             'fill': 'none',
             'stroke-dasharray': '1,1'
             }

    attribs = {
        'style': formatStyle(style),
        'height': str(height),
        'width': str(width),

        'd': f'M 175.82315,35.151321 A 32.323284,32.323284 0 0 1 143.49986,67.474606 32.323284,32.323284 0 0 1 111.17658,35.151321 32.323284,32.323284 0 0 1 143.49986,2.8280373 32.323284,32.323284 0 0 1 175.82315,35.151321 Z'
    }

    my_circle_path = etree.SubElement(parent, inkex.addNS('path', 'svg'), attribs)

    radial_gradient_object = linear_gradient_object.duplicate()

    radial_gradient_object.tag = 'radialGradient'
    radial_gradient_object_id = f'radial_gradient_{str(random.randrange(1000000, 9999999))}'
    radial_gradient_object.attrib['id'] = radial_gradient_object_id

    my_circle_path.style['fill'] = f'url(#{radial_gradient_object.get_id()})'


def make_preview_text_stops_text(self, number_of_stops):
    parent = gradient_preview_group[4]

    style = {'stroke': 'black',
             'stroke-width': '0',
             'fill': 'black',
             'font-family': 'sans-serif'
             }
    attribs = {
        'style': formatStyle(style),
        'x': '5px',
        'y': '65px'

    }

    stop_count_text = etree.SubElement(parent, inkex.addNS('text', 'svg'), attribs)
    stop_count_text.text = f'Stop Count : {number_of_stops}'


def rgba_to_rgb(self, my_image):
    transparency_color = self.options.color_picker_alpha_channel.to_rgba()
    rgb_value = [transparency_color[0], transparency_color[1], transparency_color[2]]

    rgba_bg_image = Image.new("RGBA", my_image.size, tuple(rgb_value))
    rgba_bg_image.paste(my_image, (0, 0), my_image)
    my_rgb_image = rgba_bg_image.convert(mode='RGB')

    return my_rgb_image


def make_linear_gradient(self, color_list):
    stop_index = 0

    parent = self.svg
    linear_gradient_object = etree.SubElement(parent, inkex.addNS('linearGradient', 'svg'))
    linear_gradient_object_id = f'linear_gradient_{str(random.randrange(1000000, 9999999))}'
    linear_gradient_object.attrib['id'] = linear_gradient_object_id

    svg_defs = self.svg.xpath('//svg:defs')
    svg_defs[0].append(linear_gradient_object)

    for my_stop in color_list:
        new_stop = etree.SubElement(linear_gradient_object, inkex.addNS('stop', 'svg'))
        new_stop.attrib[
            'stop-color'] = f'rgb({color_list[stop_index][0]},{color_list[stop_index][1]},{color_list[stop_index][2]})'

        if len(color_list[stop_index]) == 4:
            new_stop.attrib['offset'] = f'{color_list[stop_index][3]}'

        if len(color_list[stop_index]) > 4:
            new_stop.attrib['offset'] = f'{color_list[stop_index][4]}'
            new_stop.attrib['stop-opacity'] = str(color_list[stop_index][3] / 255)
        # stop_offset += percentage_increment
        stop_index += 1

    # Display number of stops on canvas
    make_preview_text_stops_text(self, len(linear_gradient_object.xpath('./svg:stop')))

    return linear_gradient_object


class BitmapToGradient(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--bitmap_source_combo", type=str, dest="bitmap_source_combo", default='image')

        pars.add_argument("--bitmap_to_gradient_notebook", type=str, dest="bitmap_to_gradient_notebook", default=0)

        pars.add_argument("--import_bitmap_filepath", type=str, dest="import_bitmap_filepath", default=None)

        pars.add_argument("--gradient_lower_threshold", type=float, dest="gradient_lower_threshold", default=0)

        pars.add_argument("--gradient_upper_threshold", type=float, dest="gradient_upper_threshold", default=0)

        pars.add_argument("--image_y_offset", type=float, dest="image_y_offset", default=0)

        pars.add_argument("--stop_density", type=float, dest="stop_density", default=100)

        pars.add_argument("--preview_type_combo", type=str, dest="preview_type_combo", default='rectangle')

        pars.add_argument("--gradient_type_combo", type=str, dest="gradient_type_combo", default='linear')

        pars.add_argument("--stop_slope_combo", type=str, dest="stop_slope_combo", default='linear')

        pars.add_argument("--stop_opacity_slope_combo", type=str, dest="stop_opacity_slope_combo", default='linear')

        pars.add_argument("--alpha_cb", type=str, dest="alpha_cb")

        pars.add_argument("--color_picker_alpha_channel", type=inkex.colors.Color, dest="color_picker_alpha_channel",
                          default=0)

        pars.add_argument("--optimise_cb", type=str, dest="optimise_cb")

        pars.add_argument("--reverse_cb", type=str, dest="reverse_cb")

        pars.add_argument("--greyscale_cb", type=str, dest="greyscale_cb")

        pars.add_argument("--shapeUnitFix", type=str, dest="shape_unit_fix")

        pars.add_argument("--color_picker_scanline", type=inkex.colors.Color, dest="color_picker_scanline", default=0)

        pars.add_argument("--export_csv_cb", type=str, dest="export_csv_cb")

        pars.add_argument("--export_csv_filepath", type=str, dest="export_csv_filepath", default=None)

    def effect(self):

        global found_units
        found_units = self.svg.unit

        global conversion_factor
        if self.options.shape_unit_fix == 'true':
            conversion_factor = conversions[found_units]
        else:
            conversion_factor = 1

        global import_bitmap_filepath
        import_bitmap_filepath = self.options.import_bitmap_filepath

        global gradient_preview_group
        gradient_preview_group = []

        if self.options.bitmap_source_combo == 'file':
            open_bitmap(self, import_bitmap_filepath)
        else:
            if len(self.svg.selected) > 0:
                my_object = self.svg.selected[0]
                # inkex.errormsg(my_object.TAG)
                if my_object.TAG == 'image':
                    embedded_image = my_object
                    embedded_image_to_PIL(self, embedded_image)
                else:
                    inkex.errormsg('Please Select an Image')
            else:
                inkex.errormsg('Please Select an Image')


if __name__ == '__main__':
    BitmapToGradient().run()

