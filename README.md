# Bitmap to Gradient

*** Work in progress :)

▶ Creates a simple Linear or Radial Gradient
  from a bitmap file or existing embedded image

▶ Option to export gradient rgb colour list to .csv

▶ Appears in 'Extensions>Images

▶ Would recommend using this on a blank document
  and copy / pasting the resulting objects to the
  document you are working on.
